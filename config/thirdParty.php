<?php

return [
    'gorest' => [
        'apiUrl' => 'https://gorest.co.in/',
        'path' => 'public',
        'version' => 'v2',
        'token' => 'a5659c57dd2e94ea97480db6d2abfcb41fecd00c407299872e37ed4a759f3d13',
        'timeout' => 10,
        'retry' => 5,
        'methods' => [
            'getUsers' => [
                'path' => 'users',
                'method' => 'GET',
            ],
            'deleteUser' => [
                'path' => 'users/{userId}',
                'method' => 'DELETE',
            ],
            'getPosts' => [
                'path' => 'users/{userId}/posts',
                'method' => 'GET',
            ],
            'getComments' => [
                'path' => 'posts/{postId}/comments',
                'method' => 'GET',
            ],
        ],
    ],
];

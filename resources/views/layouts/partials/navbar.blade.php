<header class="p-3 bg-dark text-white">
    <div class="container">
        <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
            <a href="/" class="d-flex align-items-center mb-2 mb-lg-0 text-white text-decoration-none">
                <svg class="bi me-2" width="40" height="32" role="img" aria-label="Bootstrap"><use xlink:href="#bootstrap"/></svg>
            </a>

            <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
                <li><a href="{{ route('dashboard') }}" class="nav-link px-2 text-white">Home</a></li>
                <li><a href="{{ route('about') }}" class="nav-link px-2 text-white">About</a></li>

                @if(Auth::user() && (Auth::user()->isAdministrator() || Auth::user()->isManager()))
                    <li><a href="{{ route('users.list') }}" class="nav-link px-2 text-white">Users</a></li>
                @endif

            </ul>

            {{Auth::user()->getEmail()}}
            <div class="text-end">
                <form action="{{ route('logout') }}" method="post">
                    @csrf
                    <button type="submit" class="btn btn-danger btn-outline-light ms-2 me-2">Logout</button>
                </form>
            </div>
        </div>
    </div>
</header>

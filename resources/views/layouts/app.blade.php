<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@section('title')@endsection</title>
    <link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet">
</head>
<body>
@auth
    @include('layouts.partials.navbar')
@endauth

<main class="container">
    @yield('content')
    @include('layouts.partials.copy')
</main>

<script src="{{ asset('/js/popper.min.js') }}"></script>
<script src="{{ asset('/js/bootstrap.min.js') }}"></script>
</body>
</html>

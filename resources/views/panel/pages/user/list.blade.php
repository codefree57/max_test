@extends('layouts.app')

@section('title', 'Users')

@section('content')
    <div class="bg-light p-5 rounded">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page">Users</li>
            </ol>
        </nav>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Status</th>
                    @if(Auth::user()->isAdministrator())
                        <th scope="col">Posts</th>
                        <th scope="col">Action</th>
                    @endif
                </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <th scope="row">{{ $user['id'] }}</th>
                    <td>{{ $user['name'] }}</td>
                    <td>{{ $user['email'] }}</td>
                    <td>{{ $user['status'] }}</td>
                    @if(Auth::user()->isAdministrator())
                        <td>
                            <a href="{{ route('posts.list', ['userId' => $user['id']]) }}" class="btn btn-primary">Show</a>
                        </td>
                        <td>
                            <form
                                action="{{ route('users.delete', ['userId' => $user['id']]) }}"
                                method="post"
                            >
                                @csrf
                                <button class="btn btn-danger " type="submit">Delete</button>
                            </form>
                        </td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

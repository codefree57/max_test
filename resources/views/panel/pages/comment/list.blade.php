@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <div class="bg-light p-5 rounded">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('users.list') }}">Users</a></li>
                <li class="breadcrumb-item"><a href="{{ route('posts.list', ['userId' => $userId]) }}">Posts</a></li>
                <li class="breadcrumb-item active" aria-current="page">Comments</li>
            </ol>
        </nav>
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Body</th>
            </tr>
            </thead>
            <tbody>
            @foreach($comments as $comment)
                <tr>
                    <th scope="row">{{ $comment['id'] }}</th>
                    <td>{{ $comment['name'] }}</td>
                    <td>{{ $comment['email'] }}</td>
                    <td>{{ $comment['body'] }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

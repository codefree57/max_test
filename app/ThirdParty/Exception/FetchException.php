<?php declare(strict_types=1);

namespace App\ThirdParty\Exception;

use Exception;

class FetchException extends Exception
{
}

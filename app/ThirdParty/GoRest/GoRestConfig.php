<?php

namespace App\ThirdParty\GoRest;

use Exception;

class GoRestConfig
{
    /**
     * @throws Exception
     */
    public static function getApiUrl(): string
    {
        $apiUrl = config('thirdParty.gorest.apiUrl');

        if (null === $apiUrl) {
            throw new Exception('Gorest - apiUrl not found');
        }

        return $apiUrl;
    }

    /**
     * @throws Exception
     */
    public static function getTimeOut(): string
    {
        $timeout = config('thirdParty.gorest.timeout');

        if (null === $timeout) {
            throw new Exception('Gorest - timeout not found');
        }

        return $timeout;
    }

    /**
     * @throws Exception
     */
    public static function getRetry(): string
    {
        $retry = config('thirdParty.gorest.retry');

        if (null === $retry) {
            throw new Exception('Gorest - retry not found');
        }

        return $retry;
    }

    /**
     * @throws Exception
     */
    public static function getPath(): string
    {
        $path = config('thirdParty.gorest.path');

        if (null === $path) {
            throw new Exception('Gorest - path not found');
        }

        return $path;
    }

    /**
     * @throws Exception
     */
    public static function getVersion(): string
    {
        $version = config('thirdParty.gorest.version');

        if (null === $version) {
            throw new Exception('Gorest - version not found');
        }

        return $version;
    }

    /**
     * @throws Exception
     */
    public static function getToken(): string
    {
        $token = config('thirdParty.gorest.token');

        if (null === $token) {
            throw new Exception('Gorest - token not found');
        }

        return $token;
    }

    /**
     * @throws Exception
     */
    public static function getUsers(): array
    {
        $users = config('thirdParty.gorest.methods.getUsers');

        if (null === $users) {
            throw new Exception('Gorest - method getUsers not found');
        }

        return $users;
    }

    /**
     * @throws Exception
     */
    public static function getDeleteUser(): array
    {
        $deleteUser = config('thirdParty.gorest.methods.deleteUser');

        if (null === $deleteUser) {
            throw new Exception('Gorest - method deleteUser not found');
        }

        return $deleteUser;
    }

    /**
     * @throws Exception
     */
    public static function getPosts(): array
    {
        $posts = config('thirdParty.gorest.methods.getPosts');

        if (null === $posts) {
            throw new Exception('Gorest - method getPosts not found');
        }

        return $posts;
    }

    /**
     * @throws Exception
     */
    public static function getComments(): array
    {
        $comments = config('thirdParty.gorest.methods.getComments');

        if (null === $comments) {
            throw new Exception('Gorest - method getComments not found');
        }

        return $comments;
    }
}

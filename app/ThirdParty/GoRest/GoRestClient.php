<?php

namespace App\ThirdParty\GoRest;

use App\ThirdParty\RestFulInterface;
use Exception;

class GoRestClient implements GoRestClientInterface
{
    private RestFulInterface $restFull;

    public function __construct(RestFulInterface $restFull)
    {
        $this->restFull = $restFull;
    }

    /**
     * @throws Exception
     */
    public function getUsers()
    {
        $version = GoRestConfig::getVersion();
        $users = GoRestConfig::getUsers();
        $path = GoRestConfig::getPath() . '/' . $version . '/' . $users['path'];
        $response = $this->restFull->send($path, $users['method']);

        return json_decode($response, true) ?? [];
    }

    /**
     * @throws Exception
     */
    public function deleteUser(int $userId)
    {
        $version = GoRestConfig::getVersion();
        $deleteUser = GoRestConfig::getDeleteUser();
        $deleteUser['path'] = str_replace('{userId}', $userId, $deleteUser['path']);
        $path = GoRestConfig::getPath() . '/' . $version . '/' . $deleteUser['path'];
        $response = $this->restFull->send($path, $deleteUser['method']);

        return json_decode($response, true) ?? [];
    }

    /**
     * @throws Exception
     */
    public function getPosts(int $userId)
    {
        $version = GoRestConfig::getVersion();
        $posts = GoRestConfig::getPosts();
        $posts['path'] = str_replace('{userId}', $userId, $posts['path']);
        $path = GoRestConfig::getPath() . '/' . $version . '/' . $posts['path'];
        $response = $this->restFull->send($path, $posts['method']);

        return json_decode($response, true) ?? [];
    }

    /**
     * @throws Exception
     */
    public function getComments(int $postId)
    {
        $version = GoRestConfig::getVersion();
        $comments = GoRestConfig::getComments();
        $comments['path'] = str_replace('{postId}', $postId, $comments['path']);
        $path = GoRestConfig::getPath() . '/' . $version . '/' .$comments['path'];
        $response = $this->restFull->send($path, 'GET', []);

        return json_decode($response, true) ?? [];
    }
}

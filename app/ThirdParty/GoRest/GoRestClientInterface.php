<?php

namespace App\ThirdParty\GoRest;

interface GoRestClientInterface
{
    public function getUsers();
    public function getPosts(int $userId);
    public function getComments(int $postId);
}

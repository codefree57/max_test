<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role(): BelongsTo
    {
        return $this->belongsTo(Role::class);
    }

    public function getFirstname()
    {
        return $this->attributes['firstname'];
    }

    public function getLastname()
    {
        return $this->attributes['lastname'];
    }

    public function getEmail()
    {
        return $this->attributes['email'];
    }

    public function getPassword()
    {
        return $this->attributes['password'];
    }

    public function isAdministrator(): bool
    {
        /** @var Role $role */
        $role = $this->role()->first();

        return Role::ADMINISTRATOR === $role->getName();
    }

    public function isManager(): bool
    {
        /** @var Role $role */
        $role = $this->role()->first();

        return Role::MANAGER === $role->getName();
    }

    public function isUser(): bool
    {
        /** @var Role $role */
        $role = $this->role()->first();

        return Role::USER === $role->getName();
    }
}

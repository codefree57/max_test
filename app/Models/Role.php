<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Role extends Model
{
    use HasFactory;

    public const ADMINISTRATOR = 'administrator';
    public const MANAGER = 'manager';
    public const USER = 'user';

    public function users(): HasMany
    {
        return $this->hasMany(User::class);
    }

    public function getId()
    {
        return $this->attributes['id'];
    }

    public function getName()
    {
        return $this->attributes['name'];
    }
}

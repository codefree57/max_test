<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function userList(User $user, string $class = User::class): bool
    {
        return $user->isAdministrator() || $user->isManager();
    }

    public function userDelete(User $user, string $class = User::class): bool
    {
        return $user->isAdministrator();
    }
}

<?php

namespace App\Providers\ThirdParty;

use App\ThirdParty\Factory\HttpClientFactory;
use App\ThirdParty\GoRest\GoRestClient;
use App\ThirdParty\GoRest\GoRestConfig;
use App\ThirdParty\HttpRestWrapper;
use Illuminate\Support\ServiceProvider;

class GoRestServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(GoRestClient::class, function($app) {
            $httpRest =  new HttpRestWrapper(
                (new HttpClientFactory()),
                GoRestConfig::getTimeOut(),
                GoRestConfig::getRetry(),
                GoRestConfig::getApiUrl(),
                GoRestConfig::getToken(),
            );

            return new GoRestClient($httpRest);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

<?php

namespace App\Utils;

use Illuminate\Support\Facades\Hash;

class Security
{
    public static function hash(string $value, $options = []): string
    {
        return Hash::make($value, $options);
    }

    public static function checkHash(string $password, string $hashedPassword): bool
    {
        return Hash::check($password, $hashedPassword);
    }
}

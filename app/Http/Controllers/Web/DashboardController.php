<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\View\View;

class DashboardController extends Controller
{
    public function index(): View
    {
        return view('panel.pages.dashboard.index');
    }

    public function about(): View
    {
        return view('panel.pages.dashboard.about');
    }
}

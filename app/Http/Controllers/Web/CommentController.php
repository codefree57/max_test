<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\ThirdParty\GoRest\GoRestClient;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class CommentController extends Controller
{
    private GoRestClient $goRestClient;

    public function __construct(GoRestClient $goRestClient)
    {
        $this->goRestClient = $goRestClient;
    }

    /**
     * @param int $userId
     * @param int $
     * @return View
     */
    public function list(int $userId, int $postId): View
    {
        /** @var User $auth */
        $auth = auth()->user();

        if (!$auth->isAdministrator()) {
            abort(Response::HTTP_UNAUTHORIZED);
        }

        try {
            $comments = $this->goRestClient->getComments($postId);

            return view('panel.pages.comment.list', compact('userId', 'comments'));
        } catch(Exception $exception) {
            Log::error($exception->getMessage());
            abort(403, 'Something wrong please try later');
        }
    }
}

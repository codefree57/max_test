<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginPost;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class AuthController extends Controller
{
    public function login(): View
    {
        return view('auth.pages.login');
    }

    public function loginPost(LoginPost $request): RedirectResponse
    {
        $credentials = $request->getCredentials();

        if (Auth::attempt($credentials, true)) {
            return redirect()->route('dashboard');
        }

        return redirect()
            ->route('login')
            ->withErrors('Check your email and password')
            ->onlyInput('email');
    }

    protected function authenticated(Request $request, $user): RedirectResponse
    {
        return redirect()->intended();
    }

    public function logout(Request $request): RedirectResponse
    {
        session()->flush();
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('login');
    }
}

<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\ThirdParty\GoRest\GoRestClient;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class PostController extends Controller
{
    private GoRestClient $goRestClient;

    public function __construct(GoRestClient $goRestClient)
    {
        $this->goRestClient = $goRestClient;
    }

    /**
     * @param int $userId
     * @return View
     */
    public function list(int $userId): View
    {
        /** @var User $auth */
        $auth = auth()->user();

        if (!$auth->isAdministrator()) {
            abort(Response::HTTP_UNAUTHORIZED);
        }

        try {
            $posts = $this->goRestClient->getPosts($userId);

            return view('panel.pages.post.list', compact('userId', 'posts'));
        } catch(Exception $exception) {
            Log::error($exception->getMessage());
            abort(403, 'Something wrong please try later');
        }
    }
}

<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\ThirdParty\GoRest\GoRestClient;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    private GoRestClient $goRestClient;

    public function __construct(GoRestClient $goRestClient)
    {
        $this->goRestClient = $goRestClient;
    }

    public function list(): View
    {
        /** @var User $auth */
        $auth = auth()->user();

        if ($auth->cannot('userList', User::class)) {
            abort(Response::HTTP_UNAUTHORIZED);
        }

        try {
            $users = $this->goRestClient->getUsers();

            return view('panel.pages.user.list', compact('users'));
        } catch(Exception $exception) {
            Log::error($exception->getMessage());
            abort(403, 'Something wrong please try later');
        }
    }

    public function delete(int $userId): RedirectResponse
    {
        /** @var User $auth */
        $auth = auth()->user();

        if ($auth->cannot('userDelete', User::class)) {
            abort(Response::HTTP_UNAUTHORIZED);
        }

        try {
            $this->goRestClient->deleteUser($userId);

            return redirect()->back();
        } catch (Exception $exception) {
            abort(403, 'Something wrong please try later');
        }
    }
}

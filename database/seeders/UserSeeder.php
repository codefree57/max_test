<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Utils\Security;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        /** @var Role $administrator */
        $administrator = Role::query()->where('name', '=', 'administrator')->first();
        /** @var Role $manager */
        $manager = Role::query()->where('name', '=', 'manager')->first();
        /** @var Role $user */
        $user = Role::query()->where('name', '=', 'user')->first();

        DB::table('users')->insert(
            [
                [
                    'firstname' => 'Admin',
                    'lastname' => 'Admin',
                    'email' => 'admin@dev.com',
                    'role_id' => $administrator->getId(),
                    'password' => Security::hash('secret'),
                ],
                [
                    'firstname' => 'Manager',
                    'lastname' => 'Manager',
                    'email' => 'manager@dev.com',
                    'role_id' => $manager->getId(),
                    'password' => Security::hash('secret'),
                ],
                [
                    'firstname' => 'User',
                    'lastname' => 'User',
                    'email' => 'user@dev.com',
                    'role_id' => $user->getId(),
                    'password' => Security::hash('secret'),
                ],
            ]
        );
    }
}

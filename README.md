# EN

## Install

### Terminal 1

```
> cd /project_path
> composer install
> ./vendor/bin/sail up
```

### Terminal 2

```
> ./vendor/bin/sail artisan migrate
> ./vendor/bin/sail artisan db:seed
```

* open http://localhost

### Users

* administrator
```
email: admin@dev.com
password: secret
```

* manager
```
email: manager@dev.com
password: secret
```
* user

```
email: user@dev.com
password: secret
```

### Note
If you need to put your `token` you can change in this file
```
config/thirdParty.php
```

# RU

## Установить

```
> cd /project_path
> composer install
> ./vendor/bin/sail up
```

### Терминал 1

```
> ./vendor/bin/sail artisan migrate
> ./vendor/bin/sail artisan db:seed
```

### Терминал 2

```
> ./vendor/bin/sail artisan migrate
> ./vendor/bin/sail artisan db:seed
```

* Вы открываете http://localhost

### Пользователи

* администратор
```
электронная почта: admin@dev.com
пароль: secret
```

* управляющий делами
```
электронная почта: manager@dev.com
пароль: secret
```
* пользователь

```
электронная почта: user@dev.com
пароль: secret
```

### Примечание
Если вам нужно поместить свой `token`, вы можете изменить его в этом файле.
```
config/thirdParty.php
```
